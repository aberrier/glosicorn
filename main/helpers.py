import socket
from socket import SocketIO
from typing import Dict, Tuple

from main.constants import MAX_HEADER_LINES, ISO_8859_1, MAX_HEADERS, BODY_CHUNK_SIZE
from main.exceptions import HeaderTooLargeError, BadRequestError


def parse_headers(fp: SocketIO) -> Dict[str, str]:
    headers = []
    while True:
        line = fp.readline(MAX_HEADER_LINES + 1).decode(ISO_8859_1)
        if len(line) > MAX_HEADER_LINES:
            raise HeaderTooLargeError
        line = line.strip("\r\n")
        if not line:
            break
        headers.append(line)
        if len(headers) > MAX_HEADERS:
            raise HeaderTooLargeError(f"Received more than {MAX_HEADERS} headers.")

    return {
        k.lower(): v
        for k, v in dict(header.split(": ", 1) for header in headers).items()
    }


def parse_body(s: socket, headers: Dict[str, str]) -> bytes:
    content_length = None
    for k, v in headers.items():
        if k.lower() == "content-length":
            try:
                content_length = int(v)
            except ValueError:
                raise BadRequestError(
                    f"Invalid Content-Length header: {content_length}"
                )
    if content_length is None:
        return bytes()
    data = bytes()
    current_length = 0
    try:
        while not content_length <= current_length:
            chunk = s.recv(BODY_CHUNK_SIZE)
            if not chunk:
                break
            else:
                data += chunk
                current_length += len(chunk)
    except socket.timeout:
        pass
    return data


def parse_version(version: str) -> Tuple[int, int]:
    if not version.startswith("HTTP/"):
        raise ValueError
    base_version_number = version.split("/", 1)[1]
    version_number = base_version_number.split(".")
    if len(version_number) != 2:
        raise ValueError
    return int(version_number[0]), int(version_number[1])
