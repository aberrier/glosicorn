from main import status
from main.constants import STATUS_CODES


class HTTPError(Exception):
    code = status.HTTP_500_INTERNAL_SERVER_ERROR

    def __init__(self, detail=None):
        self.code_detail = STATUS_CODES.get(self.code, "")
        self.detail = detail or self.code_detail
        self.message = f"({self.code}): {self.detail}"
        super().__init__(self.message)


class HeaderTooLargeError(HTTPError):
    code = status.HTTP_431_REQUEST_HEADER_FIELDS_TOO_LARGE


class BadRequestError(HTTPError):
    code = status.HTTP_400_BAD_REQUEST


class VersionNotSupportedError(HTTPError):
    code = status.HTTP_505_HTTP_VERSION_NOT_SUPPORTED


class LengthRequiredError(HTTPError):
    code = status.HTTP_411_LENGTH_REQUIRED


class ExpectationFailedError(HTTPError):
    code = status.HTTP_417_EXPECTATION_FAILED
