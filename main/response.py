from dataclasses import dataclass, field
from typing import Dict, Optional, List

from main.constants import STATUS_CODES, DEFAULT_CHARSET
from main.methods import HEAD
from main.request import HTTPRequest


@dataclass
class HTTPResponse:
    request: HTTPRequest
    version: str
    status_code: int
    body: Optional[bytes] = None
    headers: Dict[str, str] = field(default_factory=dict)

    @property
    def close_connection(self) -> Optional[bool]:
        for key, value in self.headers.items():
            if key == "connection":
                if value == "close":
                    return True
                elif value == "keep-alive":
                    return False

    def set_header(self, key: str, value: str):
        self.headers[key] = value

    def to_raw(self, charset=DEFAULT_CHARSET) -> bytes:
        message = STATUS_CODES.get(self.status_code, "")
        headers: List[bytes] = [
            f"{self.version} {self.status_code} {message}\r\n".encode(charset, "strict")
        ]
        if (
            "content-length" not in (key.lower() for key in self.headers.keys())
            and self.body
        ):
            self.set_header("Content-Length", str(len(self.body)))
        for key, value in self.headers.items():
            headers.append(f"{key}: {value}\r\n".encode(charset, "strict"))
        headers.append(b"\r\n")
        raw_response = b"".join(headers)
        if self.request and self.request.method != HEAD and self.body:
            raw_response += self.body
        return raw_response
