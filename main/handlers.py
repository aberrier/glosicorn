import datetime
import html
import logging
import os
import socket
import socketserver
import urllib.parse
import mimetypes
from pathlib import Path
from typing import Optional, Union, Tuple, List

from main import constants, status
from main.constants import (
    SERVER_VERSION,
    ROOT_DIR,
    HUMAN_DATETIME_FORMAT,
    FOLDER_RESPONSE_BODY_TEMPLATE,
    DEFAULT_MIMETYPE,
    DEFAULT_CHARSET,
)
from main.constants import MAX_HEADER_LINES, ISO_8859_1
from main.exceptions import (
    BadRequestError,
    VersionNotSupportedError,
    HTTPError,
    ExpectationFailedError,
)
from main.helpers import parse_headers, parse_version, parse_body
from main.methods import GET
from main.request import HTTPRequest
from main.response import HTTPResponse

handler = logging.StreamHandler()
formatter = logging.Formatter(
    "[{asctime}] P={process} T={thread} - {message}", style="{"
)
handler.setFormatter(formatter)
logger = logging.getLogger(__name__)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class HTTPRequestHandler(socketserver.StreamRequestHandler):
    rbufsize = 0
    error_message_format = constants.DEFAULT_ERROR_MESSAGE
    error_content_type = constants.DEFAULT_ERROR_CONTENT_TYPE
    default_request_version = constants.DEFAULT_REQUEST_VERSION
    protocol_version = "HTTP/1.1"

    def __init__(self, request, client_address, server):
        self.http_request: Optional[HTTPRequest] = None
        self.http_response: Optional[HTTPResponse] = None
        self.close_connection: bool = True
        super().__init__(request, client_address, server)

    def handle(self):
        self.close_connection = True
        self.handle_request()
        self.send_response()
        while not self.close_connection:
            self.handle_request()
            self.send_response()

    def handle_request(self):
        """
        Handle socket received to transform it as an HTTPRequest object
        and call the appropriate method with it that will create an
        HTTPResponse object to be sent back.
        """
        try:
            raw_uri_line = self.rfile.readline(MAX_HEADER_LINES + 1)
            if len(raw_uri_line) > MAX_HEADER_LINES + 1:
                self.http_response = self.create_error_response(
                    status.HTTP_414_REQUEST_URI_TOO_LONG, "Request URI too long."
                )
                return
            if not raw_uri_line:
                self.close_connection = True
                return
            try:
                self.create_request(raw_uri_line)
            except HTTPError as e:
                self.log_error(e.message)
                self.http_response = self.create_error_response(e.code, e.message)
            mname = "do_" + self.http_request.method
            if not hasattr(self, mname):
                self.http_response = self.create_error_response(
                    status.HTTP_501_NOT_IMPLEMENTED,
                    f"Unsupported method {self.http_request.method.upper()}",
                )
                return
            try:
                method = getattr(self, mname)
                # The method should returns a HTTPResponse object
                self.http_response = method()
            except HTTPError as e:
                self.http_response = self.create_error_response(e.code, e.message)
                return
            except Exception as e:
                e = HTTPError(repr(e))
                self.http_response = self.create_error_response(e.code, e.message)
                logger.error("", exc_info=True)
                return
        except socket.timeout:
            logger.error("Request timed out.", exc_info=True)
            self.close_connection = True
            return

    def is_expect_100(self) -> bool:
        """
        Examine the headers and look for an Expect directive.
        Return True if an Expect directive must be handled, False otherwise.
        This determines if handle_expect_100 must be called.
        """
        #
        expect = self.http_request.headers.get("expect", "")
        return (
            expect.lower() == "100-continue"
            and self.protocol_version >= "HTTP/1.1"
            and self.http_request.version >= "HTTP/1.1"
        )

    def match_expect_100(self) -> bool:
        """
        Override this function to decide if the request should continue
        Always returns True by default.
        """
        return True

    def handle_expect_100(self):
        """Decide what to do with an "Expect: 100-continue" header.

        If the client is expecting a 100 Continue response, we must
        respond with either a 100 Continue or a final response before
        waiting for the request body. The default is to always respond
        with a 100 Continue. You can behave differently (for example,
        reject unauthorized requests) by overriding this method.

        This method should either return True (possibly after sending
        a 100 Continue response) or send an error response and return
        False.

        """
        if self.match_expect_100():
            self.http_response = HTTPResponse(
                request=self.http_request,
                status_code=status.HTTP_100_CONTINUE,
                version=self.protocol_version,
            )
            self.send_response()
            # We remove the response to avoid sending it again
            self.http_response = None
            return True

        return False

    def send_response(self) -> bool:
        """
        Send HTTP response to socket's write buffer
        """
        if self.http_response:
            self.log_request(self.http_response.status_code)
            self.wfile.write(self.http_response.to_raw())
            if self.http_response.close_connection is not None:
                self.close_connection = self.http_response.close_connection
            return True
        self.wfile.flush()
        return False

    def create_error_response(
        self, code: int, message: str, with_body=True
    ) -> HTTPResponse:
        """
        Create a HTTPResponse object for errors happening on handler's side
        """
        self.log_error(f"[{code}] {message}")
        response = HTTPResponse(
            request=self.http_request, version=self.protocol_version, status_code=code
        )
        response.set_header("Server", self.get_server_version())
        response.set_header(
            "Date", datetime.datetime.utcnow().strftime(HUMAN_DATETIME_FORMAT)
        )
        response.set_header("Connection", "close")

        # Message body is omitted for cases described in:
        #  - RFC7230: 3.3. 1xx, 204(No Content), 304(Not Modified)
        #  - RFC7231: 6.3.6. 205(Reset Content)
        if (
            with_body
            and code >= 200
            and code
            not in (
                status.HTTP_204_NO_CONTENT,
                status.HTTP_205_RESET_CONTENT,
                status.HTTP_205_RESET_CONTENT,
                status.HTTP_304_NOT_MODIFIED,
                status.HTTP_417_EXPECTATION_FAILED,
            )
        ):

            msg = self.error_message_format % {
                "code": code,
                "message": html.escape(message, quote=False),
            }
            response.body = msg.encode(DEFAULT_CHARSET)
            response.set_header("Content-Type", self.error_content_type)
        return response

    def create_request(self, raw_line: bytes):
        """
        Create an HTTPRequest object from socket's buffer
        """
        self.close_connection = True
        version = self.default_request_version
        raw_line = raw_line.decode(ISO_8859_1).rstrip("\r\n")
        words = raw_line.split()
        if len(words) == 0:
            raise ValueError

        if len(words) >= 3:
            current_version = words[-1]
            try:
                version_number = parse_version(current_version)
            except (ValueError, IndexError):
                raise BadRequestError(f"Bad request version ({current_version})")
            if version_number >= (1, 1) and self.protocol_version >= "HTTP/1.1":
                self.close_connection = False
            if version_number >= (2, 0):
                raise VersionNotSupportedError(
                    f"Invalid HTTP version {version_number[0]}.{version_number[1]}"
                )
            version = current_version

        if not 2 <= len(words) <= 3:
            raise BadRequestError(f"Bad request syntax: {raw_line}")
        method, uri = words[:2]
        if len(words) == 2:
            self.close_connection = True
            if method.lower() != GET:
                raise BadRequestError(f"Bad HTTP/0.9 request type {method}")

        headers = parse_headers(self.rfile)
        # Examine the headers and look for an Expect directive.
        expect = headers.get("expect", "")
        if (
            expect.lower() == "100-continue"
            and self.protocol_version >= "HTTP/1.1"
            and self.http_request.version >= "HTTP/1.1"
        ):
            if not self.handle_expect_100():
                raise ExpectationFailedError

        # Read body
        body = parse_body(self.request, headers)
        self.http_request = HTTPRequest(
            version=version,
            method=method.lower(),
            headers=headers,
            uri=uri,
            body=body,
        )
        # Examine the headers and look for a Connection directive.
        # The decided behaviour can be overridden by the response.
        conntype = headers.get("connection", "")
        if conntype.lower() == "close":
            self.close_connection = True
        elif conntype.lower() == "keep-alive" and self.protocol_version >= "HTTP/1.1":
            self.close_connection = False

    def log_request(self, code: Union[int, str] = "-", size="-"):
        msg = ""
        if self.http_request:
            msg = f'"{self.http_request.method.upper()} - {self.http_request.version}"'
            if self.http_request.body:
                size = len(self.http_request.body)
        self.log_message(f"{msg} {code} {size}")

    def log_error(self, msg):
        self.log_message(msg, level=logging.ERROR)

    def log_message(self, msg, level=logging.INFO):
        msg = f"{self.address_string()} - {msg}"
        logger.log(level, msg)

    @staticmethod
    def get_server_version() -> str:
        return SERVER_VERSION

    def address_string(self) -> str:
        return self.client_address[0]


class StaticFileRequestHandler(HTTPRequestHandler):
    def __init__(self, request, client_address, server):
        self.path: Path = getattr(server, "static_dir", None)
        super().__init__(request, client_address, server)

    def get_folder_response_body(self, path: Path) -> bytes:
        pre = []
        for elem in path.iterdir():
            name = elem.name
            if elem.is_dir():
                name += "/"
            pre.append(f'<a href="{name}">{name}</a>')
            cur = len(name)
            stat_buf = os.stat(elem)

            last_modified = str(
                datetime.datetime.fromtimestamp(stat_buf.st_mtime).strftime(
                    HUMAN_DATETIME_FORMAT
                )
            ).rjust(64 - cur)
            cur += len(last_modified)
            size = "-"
            if elem.is_file():
                size = str(stat_buf.st_size)
            size = size.rjust(84 - cur)
            pre.append(f"{last_modified}{size}\n")

        relative_path = f"/{os.path.relpath(path, self.path)}/"
        return FOLDER_RESPONSE_BODY_TEMPLATE.format(
            relative_path=relative_path, pre="".join(pre)
        ).encode(DEFAULT_CHARSET)

    def read_range_header(self, value: str, path: Path) -> Optional[Tuple[int, int]]:
        """
        Read the Range directive and return the acceptable range as tuple if any.
        """
        print("toto")
        print(value)
        stat_buf = os.stat(path)
        size = stat_buf.st_size
        print(size)
        print(path.stat().st_size)
        print(os.path.getsize(path))
        unit, ranges = value.split("=")
        if unit != "bytes":
            raise ValueError(f"Invalid unit: {unit}")
        ranges = ranges.split(",")
        for r in ranges:
            start, end = r.split("-")
            start = 0 if start == "" else int(start)
            end = size if end == "" else int(end)
            if start >= 0 and end <= size:
                if start >= end != 0:
                    raise ValueError("Start greater or equal than end")
                return start, end
        raise ValueError("No valid range found")

    def get_response(self) -> HTTPResponse:
        """
        Return an HTTPResponse object used to browse and describe static files
        """
        parsed_url = urllib.parse.urlsplit(self.http_request.uri)
        uri_path = parsed_url.path
        if uri_path.startswith("/"):
            uri_path = uri_path[1:]
        path = self.path / uri_path
        if path.is_dir():
            if not parsed_url.path.endswith("/"):
                # Redirection without a treading slash
                return HTTPResponse(
                    request=self.http_request,
                    version=self.protocol_version,
                    headers={"Location": self.http_request.uri + "/"},
                    status_code=status.HTTP_301_MOVED_PERMANENTLY,
                    body=self.http_request.body,
                )
            # Display subdirectories and files inside directory
            return HTTPResponse(
                request=self.http_request,
                version=self.protocol_version,
                headers={
                    "Location": self.http_request.uri,
                    "Content-Type": "text/html",
                },
                status_code=status.HTTP_200_OK,
                body=self.get_folder_response_body(path),
            )
        if path.is_file():
            mimetype, encoding = mimetypes.guess_type(str(path))
            # Handle Range directive
            stat_buf = os.stat(path)
            start, end = 0, stat_buf.st_size
            if "range" in self.http_request.headers:
                try:
                    start, end = self.read_range_header(
                        self.http_request.headers["range"], path
                    )
                except ValueError as e:
                    logger.error("", exc_info=True)
                    return HTTPResponse(
                        request=self.http_request,
                        version=self.protocol_version,
                        status_code=status.HTTP_416_REQUESTED_RANGE_NOT_SATISFIABLE,
                        body=repr(e).encode("utf-8"),
                    )

            if not mimetype:
                mimetype = DEFAULT_MIMETYPE

            headers = {
                "Content-Type": mimetype,
                "Content-Length": stat_buf.st_size,
                "Last-Modified": datetime.datetime.fromtimestamp(
                    stat_buf.st_mtime
                ).strftime(HUMAN_DATETIME_FORMAT),
            }
            if encoding:
                headers["Content-Encoding"] = encoding
            try:
                with path.open("rb") as file:
                    file.seek(start)
                    return HTTPResponse(
                        request=self.http_request,
                        version=self.protocol_version,
                        status_code=status.HTTP_206_PARTIAL_CONTENT
                        if start != 0 or end != stat_buf.st_size
                        else status.HTTP_200_OK,
                        headers=headers,
                        body=file.read(end - start),
                    )
            except OSError:
                return self.create_error_response(
                    status.HTTP_404_NOT_FOUND, "File not found"
                )
        else:
            return self.create_error_response(
                status.HTTP_404_NOT_FOUND,
                f"{path.name} is not a file or a directory",
            )

    def do_get(self) -> HTTPResponse:
        return self.get_response()
