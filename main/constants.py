import mimetypes
import os
import sys
from pathlib import Path

from main import status, methods

MAX_HEADER_LINES = 65536
MAX_HEADERS = 100
BODY_CHUNK_SIZE = 4096
ISO_8859_1 = "iso-8859-1"
SYS_VERSION = "Python/" + sys.version.split()[0]
SERVER_VERSION = "Glosicorn/0.0.1"
DEFAULT_REQUEST_VERSION = "HTTP/0.9"
ROOT_DIR = Path(__file__).parent.parent
HUMAN_DATETIME_FORMAT = "%a, %d %b %Y %H:%M:%S GMT"


STATUS_CODES = {
    status.HTTP_100_CONTINUE: "Continue",
    status.HTTP_101_SWITCHING_PROTOCOLS: "Switching Protocols",
    status.HTTP_103_EARLY_HINTS: "Early Hints",
    status.HTTP_200_OK: "OK",
    status.HTTP_201_CREATED: "Created",
    status.HTTP_202_ACCEPTED: "Accepted",
    status.HTTP_203_NON_AUTHORITATIVE_INFORMATION: "Non-Authoritative Information",
    status.HTTP_204_NO_CONTENT: "No Content",
    status.HTTP_205_RESET_CONTENT: "Reset Content",
    status.HTTP_206_PARTIAL_CONTENT: "Partial Content",
    status.HTTP_300_MULTIPLE_CHOICES: "Multiple Choices",
    status.HTTP_301_MOVED_PERMANENTLY: "Moved Permanently",
    status.HTTP_302_FOUND: "Found",
    status.HTTP_303_SEE_OTHER: "See Other",
    status.HTTP_304_NOT_MODIFIED: "Not Modified",
    status.HTTP_307_TEMPORARY_REDIRECT: "Temporary Redirect",
    status.HTTP_308_PERMANENT_REDIRECT: "Permanent Redirect",
    status.HTTP_400_BAD_REQUEST: "Bad Request",
    status.HTTP_401_UNAUTHORIZED: "Unauthorized",
    status.HTTP_402_PAYMENT_REQUIRED: "Payment Required",
    status.HTTP_403_FORBIDDEN: "Forbidden",
    status.HTTP_404_NOT_FOUND: "Not Found",
    status.HTTP_405_METHOD_NOT_ALLOWED: "Method Not Allowed",
    status.HTTP_406_NOT_ACCEPTABLE: "Not Acceptable",
    status.HTTP_407_PROXY_AUTHENTICATION_REQUIRED: "Proxy Authentication Required",
    status.HTTP_408_REQUEST_TIMEOUT: "Request Timeout",
    status.HTTP_409_CONFLICT: "Conflict",
    status.HTTP_410_GONE: "Gone",
    status.HTTP_411_LENGTH_REQUIRED: "Length Required",
    status.HTTP_412_PRECONDITION_FAILED: "Precondition Failed",
    status.HTTP_413_REQUEST_ENTITY_TOO_LARGE: "Payload Too Large",
    status.HTTP_414_REQUEST_URI_TOO_LONG: "URI Too Long",
    status.HTTP_415_UNSUPPORTED_MEDIA_TYPE: "Unsupported Media Type",
    status.HTTP_416_REQUESTED_RANGE_NOT_SATISFIABLE: "Range Not Satisfiable",
    status.HTTP_417_EXPECTATION_FAILED: "Expectation Failed",
    status.HTTP_418_IM_A_TEAPOT: "I'm a teapot",
    status.HTTP_422_UNPROCESSABLE_ENTITY: "Unprocessable Entity",
    status.HTTP_425_TOO_EARLY: "Too Early",
    status.HTTP_426_UPGRADE_REQUIRED: "Upgrade Required",
    status.HTTP_428_PRECONDITION_REQUIRED: "Precondition Required",
    status.HTTP_429_TOO_MANY_REQUESTS: "Too Many Requests",
    status.HTTP_431_REQUEST_HEADER_FIELDS_TOO_LARGE: "Request Header Fields Too Large",
    status.HTTP_451_UNAVAILABLE_FOR_LEGAL_REASONS: "Unavailable For Legal Reasons",
    status.HTTP_500_INTERNAL_SERVER_ERROR: "Internal Server Error",
    status.HTTP_501_NOT_IMPLEMENTED: "Not Implemented",
    status.HTTP_502_BAD_GATEWAY: "Bad Gateway",
    status.HTTP_503_SERVICE_UNAVAILABLE: "Service Unavailable",
    status.HTTP_504_GATEWAY_TIMEOUT: "Gateway Timeout",
    status.HTTP_505_HTTP_VERSION_NOT_SUPPORTED: "HTTP Version Not Supported",
    status.HTTP_506_VARIANT_ALSO_NEGOTIATES: "Variant Also Negotiates",
    status.HTTP_507_INSUFFICIENT_STORAGE: "Insufficient Storage",
    status.HTTP_508_LOOP_DETECTED: "Loop Detected",
    status.HTTP_510_NOT_EXTENDED: "Not Extended",
    status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED: "Network Authentication Required",
}

METHODS = [
    methods.GET,
    methods.POST,
    methods.PUT,
    methods.HEAD,
    methods.DELETE,
    methods.OPTIONS,
    methods.TRACE,
    methods.COPY,
    methods.LOCK,
    methods.MKCOL,
    methods.MOVE,
    methods.PURGE,
    methods.PROPFIND,
    methods.PROPPATCH,
    methods.UNLOCK,
    methods.REPORT,
    methods.MKACTIVITY,
    methods.CHECKOUT,
    methods.MERGE,
    methods.M_SEARCH,
    methods.NOTIFY,
    methods.SUBSCRIBE,
    methods.UNSUBSCRIBE,
    methods.PATCH,
    methods.SEARCH,
    methods.CONNECT,
]

DEFAULT_ERROR_MESSAGE = """
<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>%(code)d - %(message)s</title>
    
    </head>
    <body>
        <h1>An unexpected error happened</h1>
        <p>Code: %(code)d</p>
        <p>Message: %(message)s.</p>
        <img src="https://thumbs.gfycat.com/AccurateUnfinishedBergerpicard.webp"/>
    </body>
</html>
"""

DEFAULT_ERROR_CONTENT_TYPE = "text/html;charset=utf-8"

FOLDER_RESPONSE_BODY_TEMPLATE = """
<html>
<head>
<title>Index of {relative_path}</title>
</head>
<body bgcolor="white">
<h1>Index of {relative_path}</h1>
<hr>
<pre>
<a href="../">../</a>
{pre}
</pre>
<hr>
</body>
</html>
"""
DEFAULT_MIMETYPE = "application/octet-stream"
DEFAULT_CHARSET = "utf-8"