from dataclasses import dataclass
from typing import Dict, Optional


@dataclass
class HTTPRequest:
    version: str
    method: str
    headers: Dict[str, str]
    uri: str
    body: Optional[bytes] = None
