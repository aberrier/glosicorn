import logging
import socketserver
from pathlib import Path

from main.handlers import StaticFileRequestHandler

logger = logging.getLogger(__name__)

socketserver.TCPServer.allow_reuse_address = True

PORT = 8080
with socketserver.TCPServer(("", PORT), StaticFileRequestHandler) as httpd:
    httpd.static_dir = Path("/home")
    print("serving at port", PORT)
    httpd.serve_forever()
